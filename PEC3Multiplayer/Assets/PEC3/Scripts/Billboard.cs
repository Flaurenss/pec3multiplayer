using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour {

    Camera mainCamera;
    /// <summary>
    /// Search the gameobject that contains the maincamera
    /// </summary>
    private void Start()
    {
        mainCamera = GameObject.Find("CameraRig").GetComponentInChildren<Camera>();
    }

    // Update is called once per frame
    void Update () {
        transform.LookAt(mainCamera.transform);
    }
}
