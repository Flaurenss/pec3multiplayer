﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour {
    public GameObject optionsMenu;
    public InputField inputNickName;
    public bool pauseTime;
    public bool hasNickName;

    public static bool isPaused = false;

    private GameObject localPlayer;
    // Use this for initialization
    void Start()
    {
        if (hasNickName)
        {
            inputNickName.text = PlayerPrefs.GetString("UserName");
        }
    }

    private void LookForLocalPlayer()
    {
        foreach (GameObject tank in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (tank.GetComponent<NetworkIdentity>().isLocalPlayer)
            {
                localPlayer = tank;
                return;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Continue();
        }

        if (optionsMenu.activeSelf && pauseTime)
        {
            Time.timeScale = 0;
            isPaused = true;
        }
        else
        {
            Time.timeScale = 1;
            isPaused = false;
        }
    }

    public void Continue()
    {
        optionsMenu.SetActive(!optionsMenu.activeSelf);
    }

    public void UpdateNickname()
    {
        string newNick = inputNickName.text;

        PlayerPrefs.SetString("UserName", newNick);
        LookForLocalPlayer();
        localPlayer.GetComponent<NamesManager>().CmdSetPlayerName(newNick);
    }

    public void ExitToMenu()
    {
        if (!pauseTime)
        {
            NetworkManager.singleton.StopClient();
            NetworkManager.singleton.StopHost();
            NetworkManager.singleton.StopServer();
        }
        SceneManager.LoadScene("MainMenu");
    }

    public void ExitGame()
    {
        if (!pauseTime)
        {
            NetworkManager.singleton.StopClient();
            NetworkManager.singleton.StopHost();
            NetworkManager.singleton.StopServer();
        }
        Application.Quit();
    }

}
