﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking.Match;

public class RoomListItem : MonoBehaviour {

    public delegate void JoinRoomDelegate(MatchInfoSnapshot _match);
    //Instance of the delegate
    private JoinRoomDelegate joinRoomDelegate;

    [SerializeField]
    private Text roomListItem;
    private Text userName;
    private MatchInfoSnapshot match;

    /// <summary>
    /// Set up a list item = button to join a room/game
    /// </summary>
    /// <param name="_match"></param>
    /// <param name="_joinRoomDelegate"></param>
    public void Setup(MatchInfoSnapshot _match, JoinRoomDelegate _joinRoomDelegate)
    {
        match = _match;
        joinRoomDelegate = _joinRoomDelegate;
        roomListItem.text = match.name + " (" + match.currentSize + "/" + match.maxSize +")";
    }
    /// <summary>
    /// When JoinGame is called, it will execute any function that was passed by paramater on Setup
    /// </summary>
    public void JoinGame()
    {
        joinRoomDelegate.Invoke(match);
    }

}
