﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletWAN : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        GameObject hit = collision.gameObject;
        HealthWAN health = hit.GetComponent<HealthWAN>();
        if(health != null)
        {
            health.TakeDamage(10);
        }
        Destroy(gameObject);
    }
}
