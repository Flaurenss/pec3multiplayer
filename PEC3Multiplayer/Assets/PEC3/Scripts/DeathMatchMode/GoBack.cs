﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoBack : MonoBehaviour {
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameObject networkManager = GameObject.Find("NetworkManager");
            Destroy(networkManager);
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void ReturnToMainMenu()
    {
        GameObject networkManager = GameObject.Find("NetworkManager");
        Destroy(networkManager);
        SceneManager.LoadScene("MainMenu");
    }

}
