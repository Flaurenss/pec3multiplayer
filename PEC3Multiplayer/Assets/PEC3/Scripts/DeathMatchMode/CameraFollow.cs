﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform playerTransform;
    public int depth = -50;

	
	// Update is called once per frame
	void Update ()
    {
	    if(playerTransform != null)
        {
            transform.position = playerTransform.position;
        }
	}

    public void SetTarget(Transform target)
    {
        playerTransform = target;
    }
}
