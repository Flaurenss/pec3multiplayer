﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HostGame : MonoBehaviour
{
    //small data than int
    private uint roomSize = 6;
    private string roomName;
    private NetworkManager networkManager;
    [SerializeField]
    private Text userName;

    private void Start()
    {
        networkManager = NetworkManager.singleton;
        if(networkManager.matchMaker == null)
        {
            //enable the match maker option
            networkManager.StartMatchMaker();
        }
    }

    public void SetRoomName(string name)
    {
        roomName = name;
    }

    public void CreateRoom()
    {
        if(roomName != "" && roomName != null)
        {
            Debug.Log("Creating room: " + roomName + " with room for " + roomSize + " players.");
            //Create the room
            PlayerPrefs.SetString("UserName",userName.text);
            networkManager.matchMaker.CreateMatch(roomName, roomSize, true, "", "","", 0, 0, networkManager.OnMatchCreate);
        }
    }
}
