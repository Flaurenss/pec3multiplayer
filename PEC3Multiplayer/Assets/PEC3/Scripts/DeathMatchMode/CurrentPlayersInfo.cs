﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentPlayersInfo : MonoBehaviour {

    public Text text;
    public Tempus tmp;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        text.text = "Current players: " + tmp.PlayerNum;
	}
}
