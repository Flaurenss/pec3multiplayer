﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System;

public class JoinGame : MonoBehaviour {

    List<GameObject> roomList = new List<GameObject>();
    private NetworkManager networkManager;
    [SerializeField]
    private Text userName;
    [SerializeField]
    private Text status;
    [SerializeField]
    private GameObject roomListItem;
    [SerializeField]
    private Transform roomListParent;

    private void Start()
    {
        networkManager = NetworkManager.singleton;
        if (networkManager.matchMaker == null)
        {
            //enable the match maker option
            networkManager.StartMatchMaker();
        }

        RefreshRoomList();
    }
    /// <summary>
    /// Get a list with all the current matches
    /// </summary>
    public void RefreshRoomList()
    {
        ClearRoomList();
        networkManager.matchMaker.ListMatches(0, 20, "",false, 0, 0, OnMatchList);
        status.gameObject.SetActive(true);
        status.text = "Loading...";
    }
    /// <summary>
    /// Set up all the mathes if theres any, if not prompt info message
    /// </summary>
    /// <param name="success"></param>
    /// <param name="extendedInfo"></param>
    /// <param name="matchList"></param>
    private void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {
        status.text = "";

        if(!success || matchList == null)
        {
            status.gameObject.SetActive(true);
            status.text = "Couldn't get room list";
            return;
        }

        foreach(MatchInfoSnapshot match in matchList)
        {
            GameObject roomListNewItem = Instantiate(roomListItem);
            roomListNewItem.transform.SetParent(roomListParent, false);
            //Set properties from new item as well as setting the callback in order to join the game
            RoomListItem _roomListItem = roomListNewItem.GetComponent<RoomListItem>();
            if(_roomListItem != null)
            {
                _roomListItem.Setup(match, JoinRoom);
            }
            roomList.Add(roomListNewItem);
        }

        if(roomList.Count == 0)
        {
            status.gameObject.SetActive(true);
            status.text = "No games available";
        }
        else
        {
            status.gameObject.SetActive(false);
        }
    }
    /// <summary>
    /// Function called from the delegate defined on the JoinRoom button
    /// </summary>
    /// <param name="match"></param>
    public void JoinRoom(MatchInfoSnapshot match)
    {
        PlayerPrefs.SetString("UserName", userName.text);
        Debug.Log("Joining the room " + match.name);
        networkManager.matchMaker.JoinMatch(match.networkId, "", "", "", 0, 0, networkManager.OnMatchJoined);
        status.gameObject.SetActive(true);
        status.text = "Joining...";
        ClearRoomList();
    }

    private void ClearRoomList()
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            Destroy(roomList[i]);
        }
        //also Erase references
        roomList.Clear();
    }
}
