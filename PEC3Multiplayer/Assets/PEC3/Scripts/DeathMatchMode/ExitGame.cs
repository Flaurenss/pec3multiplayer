﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class ExitGame : NetworkBehaviour
{
    Tempus temp;
    private NetworkManager networkManager;

    NamesManager namesManager;

    private void Start()
    {
        temp = FindObjectOfType<Tempus>();
        namesManager = GetComponent<NamesManager>();
        networkManager = NetworkManager.singleton;
    }

    // Checks if the ESC key is pressed in order to leave the current room
    void Update ()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            //TODO decrease player num on server
            if(isLocalPlayer)
            {
                temp.CmdDecreasePlayerNum(1);
            }
            //ExitRoom();
        }   
	}

    private void ExitRoom()
    {
        MatchInfo matchInfo = networkManager.matchInfo;
        networkManager.matchMaker.DropConnection(matchInfo.networkId, matchInfo.nodeId, 0, networkManager.OnDropConnection);
        networkManager.StopClient();
    }
}
