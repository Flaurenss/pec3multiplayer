﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NamesManager : NetworkBehaviour
{
    public Text NickName;
    [SyncVar(hook = "NameChanged")]
    public string name;
    [SyncVar]
    public int playerCount;

    Tempus temp;
    TankController player;

    private void Awake()
    {
        temp = FindObjectOfType<Tempus>();
        player = GetComponent<TankController>();
    }

    private void Start()
    {
        /*
        if (temp == null)
            return;
        temp.CmdAddPlayer(GameObject.FindGameObjectsWithTag("Player").Length);*/
    }

    public override void OnStartLocalPlayer()
    {
        if (temp == null)
            return;

        playerCount = GameObject.FindGameObjectsWithTag("Player").Length;//temp.PlayerNum + 1;
        var name = PlayerPrefs.GetString("UserName");
        
        if (name == string.Empty)
        {
            //var playerNum = temp.PlayerNum + 1;
            var defaultName = "Player" + playerCount; //playerNum;
            CmdSetPlayerName(defaultName);
        }
        else
        {
            CmdSetPlayerName(name);
        }
        CmdSetTeam();
    }

    /// <summary>
    /// Init the other clients (not local) with the correct state of the server
    /// </summary>
    public override void OnStartClient()
    {
        NameChanged(name);
    }

    [Command]
    public void CmdSetPlayerName(string newName)
    {
        name = newName;
    }

    [Command]
    void CmdSetTeam()
    {
        int num;
        if (temp.PlayerNum == 0)
            num = 1;
        else
            num = temp.PlayerNum;

        if (num % 2 == 0)
            player.team = 1;
        else
            player.team = 2;
    }

    public void NameChanged(string newName)
    {
        name = newName;
        NickName.text = newName;
    }
    
 

}
