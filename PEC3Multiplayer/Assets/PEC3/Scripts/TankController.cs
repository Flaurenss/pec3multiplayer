﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TankController : NetworkBehaviour {

    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public Color materialColor;

    [SyncVar]
    public int team;
    

    // Update is called once per frame
    void Update ()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
           CmdFire();
        }
        float x = Input.GetAxis("Horizontal1") * Time.deltaTime * 150.0f;
        float z = Input.GetAxis("Vertical1") * Time.deltaTime * 3.0f;
        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

    }

    //Run this on the server but called by the client who is doing the action of shooting
    [Command]
    private void CmdFire()
    {
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, 
            bulletSpawn.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;
        bullet.GetComponent<BulletLAN>().teamShot = team;

        NetworkServer.Spawn(bullet);
        Destroy(bullet, 2.0f);
    }
    public override void OnStartLocalPlayer()
    {
        foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
        {
            child.material.color = materialColor;
        }
        GameObject.Find("CameraRig").GetComponent<CameraFollow>().SetTarget(gameObject.transform);
    }
}
