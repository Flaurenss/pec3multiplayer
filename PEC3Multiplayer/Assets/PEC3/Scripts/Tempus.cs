﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Tempus : NetworkBehaviour
{
    [SyncVar(hook = "OnChangeValue")]
    public int PlayerNum;
    [SyncVar]
    public bool PlayerToSubstract = false;
    [HideInInspector] public bool DecreaseNum = false;
    

    private void Start()
    {
        //PlayerNum = 0;
    }

    private void Update()
    {
        int value = GameObject.FindGameObjectsWithTag("Player").Length;
        PlayerNum = value;
    }

    [Command]
    public void CmdAddPlayer(int value)
    {
       
        PlayerNum = value;
    }

    [Command]
    public void CmdDecreasePlayerNum(int value)
    {
        PlayerNum -=  value;

    }

    [Command]
    public void CmdPlayerToSubstract(bool value)
    {
        PlayerToSubstract = value;
    }

    public void MinusPlayer(int value)
    {
        PlayerNum -= value;
        //Try to send to the server
    }

    void OnChangeValue(int value)
    {
        PlayerNum = value;
    }
}
