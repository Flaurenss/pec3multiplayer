﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyMenu : NetworkBehaviour {

    public Text ipAddress;
    public Text UserName;
    public GameMode CurrentMode;

    LobbyDiscovery discovery;
    NamesManager namesManager;
    string externalIP;

    Toggle friendlyFireToggle;
    bool friendlyFire;
    private void Awake()
    {
        discovery = FindObjectOfType<LobbyDiscovery>();
        namesManager = FindObjectOfType<NamesManager>();

        friendlyFire = true;
    }

    // Use this for initialization
    void Start ()
    {
        //StartCoroutine(NetworkSetup());
        PlayerPrefs.SetString("UserName", string.Empty);
        StopDiscovery();
        friendlyFireToggle = GameObject.Find("FriendlyFireToggle").GetComponent<Toggle>();
    }
	//Start game as host and client
    public void CreateGame()
    {
        StopDiscovery();
        BulletLAN.isFriendlyFireActive = friendlyFire;
        discovery.StartAsServer();
        NetworkManager.singleton.StartHost();
        AddressData();
        SetPlayerName();
        if (CurrentMode == GameMode.DeathMatch)
        {
            AnnounceGame();
        }
    }

    public void SetPlayerName()
    {
        PlayerPrefs.SetString("UserName", UserName.text);
    }

    public void AnnounceGame()
    {
        //Hack in order to get externalIP
        //var tmp = Network.Connect("www.google.com");
        string myIp = Network.player.ipAddress;
        
        GameObject chatClient = GameObject.Find("ChatClient");
        WebSocketClient ws = chatClient.GetComponent<WebSocketClient>();
        ws.inputField.text = string.Empty;
        ws.inputField.text = "My game is open, join me! " + myIp;
        ws.SendText();
    }

    public IEnumerator NetworkSetup()
    {
        Network.Connect("www.google.com");
        float time = 0;
        while (Network.player.externalIP == "UNASSIGNED_SYSTEM_ADDRESS")
        {
            time += Time.deltaTime + 0.01f;

            if (time > 10)
            {
                Debug.LogError(" Unable to obtain external ip: Are you sure your connected to the internet");
            }
            yield return new WaitForSeconds(1);
        }
        externalIP = Network.player.externalIP;
        Network.Disconnect();
    }

    //Start game as client
    public void JoinGame()
    {
        StopDiscovery();
        // In order to allow LAN connectivity ** ONLY ** this line should be active
        //discovery.StartAsClient();
        //In order to allow localhost multiplayer (multiple instances of the game running in just one computer, ** ONLY ** this line should be active

         NetworkManager.singleton.StartClient();
         AddressData();
    }

    public void JoinIpGame()
    {
        SetPlayerName();
        Network.Connect(ipAddress.text, Network.player.port);
        NetworkManager.singleton.StartClient();
    }

    public void StartServer()
    {
        StopDiscovery();
        discovery.StartAsServer();
        NetworkManager.singleton.StartServer();
        AddressData();
    }

    private void StopDiscovery()
    {
        if(discovery.running)
        {
            discovery.StopBroadcast();
        }
    }

    private void AddressData()
    {
        // Obsolete in Unity 2018.3
        Debug.Log("Network ip: " + Network.natFacilitatorIP);

        // Obsolete in Unity 2018.3
        Debug.Log("Network port: " + Network.natFacilitatorPort);

        Debug.Log("Network ip: " + NetworkManager.singleton.networkAddress);
        Debug.Log("Network port: " + NetworkManager.singleton.networkPort);

        // Obsolete in Unity 2018.3
        Debug.Log("Networkplayer ip: " + Network.player.ipAddress);

        // Obsolete in Unity 2018.3
        Debug.Log("Networkplayer port: " + Network.player.port);

        // Obsolete in Unity 2018.3
        Debug.Log("Networkplayer external ip: " + Network.player.externalIP);

        // Obsolete in Unity 2018.3
        Debug.Log("Networkplayer external port: " + Network.player.externalPort);

        Debug.Log("//////////////");
    }

    public void SetFriendlyFire()
    {
        friendlyFire = friendlyFireToggle.isOn;
    }

    public enum GameMode
    {
        DeathMatch,
        Local
    }


}
