﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ColorsManager : NetworkBehaviour {

    [SyncVar]
    private Color playerColor;

    TankController tankController;

    private void Awake()
    {
        tankController = GetComponent<TankController>();
    }

    // Update is called once per frame
    void Update () {

        if (!isServer)
            return;

        RpcGetColor();
    }
    

    public override void OnStartLocalPlayer()
    {
        CmdChangeColor();
    }

    [ClientRpc]
    private void RpcGetColor()
    {
        foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
        {
            child.material.color = playerColor;
        }
    }

    [Command]
    public void CmdChangeColor()
    {

        switch (tankController.team)
        {
            case 1:
                playerColor = Color.green;
                break;
            case 2:
                playerColor = Color.red;
                break;
            default:
                playerColor = Color.blue;
                break;
        }
    }

    
}
