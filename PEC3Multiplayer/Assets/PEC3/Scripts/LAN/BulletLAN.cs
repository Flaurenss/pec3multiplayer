﻿using UnityEngine;

public class BulletLAN : MonoBehaviour {

    public int teamShot;

    public static bool isFriendlyFireActive = true;
    private void OnCollisionEnter(Collision collision)
    {
        GameObject hit = collision.gameObject;
        HealthWAN health = hit.GetComponent<HealthWAN>();
        TankController tc = hit.GetComponent<TankController>();
        if (health != null && tc != null)
        {
            if(isFriendlyFireActive)
            {
                health.TakeDamage(10);
            }
            else if (tc.team != teamShot || teamShot == 0)
            {
                health.TakeDamage(10);
            }
            
                
        }
        Destroy(gameObject);
    }
}
