﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuSelector : MonoBehaviour {

	public void LocalGame()
    {
        SceneManager.LoadScene("_Complete-Game");
    }

    public void LANGame()
    {
        SceneManager.LoadScene("Lobby");
    }

    public void WANGame()
    {
        SceneManager.LoadScene("LobbyPEC3");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ToMainMenu()
    {
        GameObject networkManager = GameObject.Find("NetworkManager");
        Destroy(networkManager);
        SceneManager.LoadScene("MainMenu");
    }
}
