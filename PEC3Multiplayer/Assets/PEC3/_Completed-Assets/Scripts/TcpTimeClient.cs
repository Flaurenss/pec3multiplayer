﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class TcpTimeClient : MonoBehaviour
{
    private string host = "127.0.0.1";
    private int port = 13;

    // Start is called before the first frame update
    void Start()
    {
        try
        {
            TcpClient client = new TcpClient(host, port);
            NetworkStream ns = client.GetStream();
            byte[] b = new byte[1024];
            int bytesRead = ns.Read(b, 0, b.Length);
            Debug.Log(Encoding.ASCII.GetString(b, 0, bytesRead));
            client.Close();
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }
}
