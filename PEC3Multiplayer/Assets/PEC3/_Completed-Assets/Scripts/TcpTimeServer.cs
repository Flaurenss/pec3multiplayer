﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.Text;
using System;
using System.Net;

public class TcpTimeServer : MonoBehaviour
{
    private int port = 13;

    // Start is called before the first frame update
    void Start()
    {
        bool done = false;
        IPAddress localAddr = IPAddress.Parse("127.0.0.1");
        TcpListener listener = new TcpListener(localAddr, port);
        //Start listening to client reequest
        listener.Start();
        while (!done)
        {
            Debug.Log("Waiting for connection...");
            TcpClient client = listener.AcceptTcpClient();
            Debug.Log("Connection accepted");
            NetworkStream ns = client.GetStream();
            byte[] byteTime = Encoding.ASCII.GetBytes(DateTime.Now.ToString());
            try
            {
                ns.Write(byteTime, 0, byteTime.Length);
                ns.Close();
                client.Close();
                //done = true;
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
        listener.Stop();
    }
}
